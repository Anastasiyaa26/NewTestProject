public abstract class User implements IUser {
    private String name;
    private int age;
    private boolean male ;

     User(String name) {
        this.name = name;
    }

    User(String name, int age, boolean male) {
        this.name = name;
        this.age = age;
        this.male = male;
    }

     int getAge() {
        return age;
    }

     void setAge(int age) {
        this.age = age;
    }

   abstract void print();


}





